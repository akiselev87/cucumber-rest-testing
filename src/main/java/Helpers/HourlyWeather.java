package Helpers;

public class HourlyWeather {
    String time;
    String precipIntensity;
    String precipProbability;

    public String getTime() {
        return time;
    }

    public String getPrecipIntensity() {
        return precipIntensity;
    }

    public String getPrecipProbability() {
        return precipProbability;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setPrecipIntensity(String precipIntensity) {
        this.precipIntensity = precipIntensity;
    }

    public void setPrecipProbability(String precipProbability) {
        this.precipProbability = precipProbability;
    }
}
