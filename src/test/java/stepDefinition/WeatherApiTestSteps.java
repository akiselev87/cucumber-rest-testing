package stepDefinition;

import Helpers.HourlyWeather;
import com.google.gson.*;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import utilities.Log4jHandler;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class WeatherApiTestSteps {
    Response response = null;
    String  restApiUrl = null;
    JsonPath jsonPathEvaluator = null;

    @Before
    public void startDrivers() throws IOException {
        Log4jHandler.getLogger().info("Class execution begins: " + this.getClass().getName());
    }

    @After
    public void tearDown() throws IOException {
        Log4jHandler.Add_Log.info("Class execution tear down: " + this.getClass().getName());
        response = null;
        restApiUrl = null;
        jsonPathEvaluator = null;
    }

    @Given("^rest api$")
    public void restApi() throws Throwable {
        Log4jHandler.startCucuStep("rest api");

        // Specify the base URL to the RESTful web service
        // my personal API key is here
        restApiUrl = "https://api.darksky.net/forecast/449b03d38ccde29fa1519c104d91e856";

        Log4jHandler.endCucuStep("rest api");
    }

    @And("^rest api parameters$")
    public void restApiParameters() throws Throwable {
        Log4jHandler.startCucuStep("rest api parameters");
        // another option to form rest api call after forming RestAssured.baseURI
        // Response response = httpRequest.request(Method.GET, "/somethingElse");
        restApiUrl = restApiUrl + "/37.8267,-122.4233";
        Log4jHandler.endCucuStep("rest api parameters");
    }

    @And("^rest api parameters with sql injection$")
    public void restApiParametersWithSqlInjection() throws Throwable {
        Log4jHandler.startCucuStep("rest api parameters with sql injection");

        restApiUrl = restApiUrl + "/37.8267,-122.4233%20OR%201=1";

        Log4jHandler.endCucuStep("rest api parameters with sql injection");
    }

    @When("^I \"([^\"]*)\" rest api execution result$")
    public void iRestApiExecutionResult(String method) throws Throwable {
        Log4jHandler.startCucuStep(method + " rest api execution result");

        RestAssured.baseURI = restApiUrl;

        // Get the RequestSpecification of the request that you want to sent
        // to the server. The server is speci
        // fied by the BaseURI that we have
        // specified in the above step.
        RequestSpecification httpRequest = RestAssured.given();

        // Make a request to the server by specifying the method Type and the method URL.
        // This will return the Response from the server. Store the response in a variable.
        if (method.equals("GET")) {
            response = httpRequest.request(Method.GET);
        }
        else if (method.equals("POST")) {
            response = httpRequest.request(Method.POST);
        }
        else{
            response = httpRequest.request(Method.GET);
        }

        Log4jHandler.endCucuStep(method + " rest api execution result");
    }

    @Then("^I should get status code (\\d+)$")
    public void iShouldGetStatusCode(int expectedCode) throws Throwable {
        Log4jHandler.startCucuStep("I should get status code " + expectedCode);
        int statusCode = response.getStatusCode();
        Log4jHandler.Add_Log.info("Status code: " + statusCode);
        Assert.assertEquals("Expect " + expectedCode, expectedCode, statusCode);

        //String statusLine = response.getStatusLine();
        //  Assert.assertEquals(statusLine, "HTTP/1.1 200 OK");

        // Reader header of a give name. In this line we will get
        // Header named Server
        //String serverType =  response.header("Server");
        //System.out.println("Server value: " + serverType);
        //Assert.assertEquals(serverType, "nginx/1.12.1");

        Log4jHandler.endCucuStep("I should get status code " + expectedCode);
    }

    @And("^I should get \"([^\"]*)\" text at content type$")
    public void iShouldGetTextAtContentType(String contentType) throws Throwable {
        Log4jHandler.startCucuStep("I should get " + contentType + " text at content type");

        // Reader header of a give name. In this line we will get
        // Header named Content-Type
        String contentTypeFact = response.header("Content-Type");
        Log4jHandler.Add_Log.info("Content-Type value: " + contentTypeFact);
        Assert.assertTrue(contentTypeFact.contains(contentType));

        Log4jHandler.endCucuStep("I should get " + contentType + " text at content type");
    }

    @And("^I should get \"([^\"]*)\" content encoding$")
    public void iShouldGetContentEncoding(String contentEncoding) throws Throwable {
        Log4jHandler.startCucuStep("I should get " + contentEncoding + " content encoding");

        // Reader header of a give name. In this line we will get
        // Header named Content-Encoding
        String contentEncodingFact = response.header("Content-Encoding");
        Log4jHandler.Add_Log.info("Content-Encoding: " + contentEncodingFact);
        Assert.assertEquals("Waiting for " + contentEncoding,contentEncoding,contentEncodingFact);

        Log4jHandler.endCucuStep("I should get " + contentEncoding + " content encoding");
    }

    @And("^I should get \"([^\"]*)\" in body as string$")
    public void iShouldGetInBodyAsString(String textInBody) throws Throwable {
        Log4jHandler.startCucuStep("I should get " + textInBody + " in body as string");

        // Retrieve the body of the Response
        ResponseBody body = response.getBody();

        // By using the ResponseBody.asString() method, we can convert the  body
        // into the string representation.
        String bodyAsString = body.asString();
        Log4jHandler.Add_Log.info("Response Body is: " + bodyAsString);
        Assert.assertEquals("Responce body content",bodyAsString.contains(textInBody), true);

        Log4jHandler.endCucuStep("I should get " + textInBody + " in body as string");
    }

    @And("^I should get \"([^\"]*)\" value as \"([^\"]*)\"$")
    public void iShouldGetValueAs(String keyword, String keywordValue) throws Throwable {
        Log4jHandler.startCucuStep("I should get " + keyword + " value as " + keywordValue);

        // First get the JsonPath object instance from the Response interface
        jsonPathEvaluator = response.jsonPath();

        // Then simply query the JsonPath object to get a String value of the node
        // specified by JsonPath: City (Note: You should not put $. in the Java code)
        String series = jsonPathEvaluator.get(keyword);

        Log4jHandler.Add_Log.info("Series received from Response " + series);
        Assert.assertTrue(series.contains("Los_Angeles"));

        Log4jHandler.endCucuStep("I should get " + keyword + " value as " + keywordValue);
    }

    @And("^I should get (\\d+) \"([^\"]*)\" keywords$")
    public void iShouldGetKeywords(int expectedAmount, String keyword) throws Throwable {
        Log4jHandler.startCucuStep("I should get " + expectedAmount + " " + keyword + " keywords");

        jsonPathEvaluator = response.jsonPath();
        List<String> hours = jsonPathEvaluator.getList(keyword);
        Assert.assertTrue(expectedAmount == hours.size());

        Log4jHandler.startCucuStep("I should get " + expectedAmount + " " + keyword + " keywords");
    }

    @And("^I should get error text$")
    public void iShouldGetErrorText() throws Throwable {
        Log4jHandler.startCucuStep("I should get error text");

        ResponseBody body = response.getBody();
        String bodyAsString = body.asString();
        Log4jHandler.Add_Log.info("Response Body is: " + bodyAsString);
        Assert.assertTrue(bodyAsString.contains("The given location (or time) is invalid."));

        Log4jHandler.endCucuStep("I should get error text");
    }

    @And("^I should deserialize result just to know how to do that$")
    public void iShouldDeserializeResultJustToKnowHowToDoThat() throws Throwable {
        Log4jHandler.startCucuStep("I should deserialize result just to know how to do that");
        //HourlyWeather returnedArtwork = response.getBody().as(HourlyWeather.class);
        //List<HourlyWeather> returnedArtworks = Arrays.asList(response.getBody().as(HourlyWeather[].class));
        //jsonPathEvaluator = response.body("minutely.data");

        // get only part of my nested json
        // I would liek ot get this part as an array list of HourlyWeather.class objects, not
        // as ArrayList of HashMaps
        String stringOfRequiredJSON = response.jsonPath().getJsonObject("minutely.data").toString();

        //https://futurestud.io/tutorials/gson-getting-started-with-java-json-serialization-deserialization
        //https://stackoverflow.com/questions/9598707/gson-throwing-expected-begin-object-but-was-begin-array
        Gson gson = new Gson();
        HourlyWeather[] hourlyWeatherForecast = gson.fromJson(stringOfRequiredJSON, HourlyWeather[].class);

        System.out.println("Print hourly weather forecast");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
        for (HourlyWeather forecast: hourlyWeatherForecast) {

            long unixSeconds = Long.parseLong(forecast.getTime());
            // convert seconds to milliseconds
            Date date = new Date(unixSeconds*1000L);
            // give a timezone reference for formatting (see comment at the bottom)
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
            String formattedDate = dateFormat.format(date);

            System.out.println("Time: " + formattedDate);
            System.out.println("Precip Intensity: " + forecast.getPrecipIntensity());
            System.out.println("Precip Probability: " + forecast.getPrecipProbability());
        }
        Log4jHandler.endCucuStep("I should deserialize result just to know how to do that");
    }
}