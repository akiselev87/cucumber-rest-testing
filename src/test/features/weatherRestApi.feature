Feature: As a weather rest api user I would like to get weather forecast based on parameters

  #read me
  #http://toolsqa.com/rest-assured/configure-eclipse-with-rest-assured/
  #http://james-willett.com/2015/06/rest-assured-extract-json-response/
  #http://www.baeldung.com/rest-assured-tutorial
  #https://sqa.stackexchange.com/questions/20001/how-to-test-the-rest-apis-effectively-complete
  #https://www.quora.com/How-do-you-approach-writing-test-suites-for-REST-APIs

  @positive
  Scenario: validate darksky one day weather forecat rest api based on responce code and headers
    Given rest api
    And rest api parameters
    When I "GET" rest api execution result
    Then I should get status code 200
    And I should get "json" text at content type
    And I should get "gzip" content encoding
    And I should get "latitude" in body as string
    And I should get "timezone" value as "Los_Angeles"

  @positive
  Scenario: validate darksky one day weather forecat rest api based on number of rows returned
    Given rest api
    And rest api parameters
    When I "GET" rest api execution result
    Then I should get status code 200
    And I should get "json" text at content type
    And I should get "gzip" content encoding
    And I should get "latitude" in body as string
    And I should get 49 "hourly.data.time" keywords

  @positive
  Scenario: validate darksky one day weather forecat rest api based on rest api values
    Given rest api
    And rest api parameters
    When I "GET" rest api execution result
    Then I should get status code 200
    And I should get "json" text at content type
    And I should get "gzip" content encoding
    And I should get "latitude" in body as string
    And I should deserialize result just to know how to do that

  @negative
  Scenario: validate darksky one day weather forecat rest api based on wrong get
    Given rest api
    #https://api.darksky.net/forecast/449b03d38ccde29fa1519c104d91e856/37.8267,-122.4233 OR 1=1
    #{"code":400,"error":"The given location (or time) is invalid."}
    And rest api parameters with sql injection
    When I "GET" rest api execution result
    Then I should get status code 400
    And I should get error text